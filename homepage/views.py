from django.shortcuts import render

# Create your views here.
def homepage(request):
    return render(request, 'Homepage.html')

def about(request):
    return render(request, 'About.html')

def edu(request):
    return render(request, 'Education.html')

def skills(request):
    return render(request, 'Skills.html')

def exp(request):
    return render(request, 'Experiences.html')

def contact(request):
    return render(request, 'Contact.html')

def swot(request):
    return render(request, 'Swot.html')
