from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('about/', views.about, name='about'),
    path('edu/', views.edu, name='edu'),
    path('skills/', views.skills, name='skills'),
    path('exp/', views.exp, name='exp'),
    path('contact/', views.contact, name='contact'),
    path('about/swot/', views.swot, name='swot'),
]
